﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using Livingstone.DBLib;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using MicrosoftGraphCore.AuthProcess;
using MicrosoftGraphCore.SubscriptionService;
using Newtonsoft.Json;
using MicrosoftGraphCore.Services;
using Microsoft.Graph.CallRecords;
using Newtonsoft.Json.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MicrosoftGraphCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunicationsController : ControllerBase
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();

        private static volatile Dictionary<string, string> usersIdEmail = new Dictionary<string, string>();
        private static volatile Dictionary<string, string> usersEmailId = new Dictionary<string, string>();
        private readonly GraphServiceClient _graphClientUser;
        private readonly GraphServiceClient _graphClientDaemon;
        private readonly ILogger<CommunicationsController> _logger;

        private static readonly object subLoadLock = new object();
        private static readonly object recordInsertLock = new object();
        private static readonly object recordDetailsAndSubsLock = new object();

        private static Dictionary<string, Subscription> CommSubscriptions = new Dictionary<string, Subscription>();

        private static Timer subscriptionTimer = null;
        private static Timer recordLoggingTimer = null;

        public CommunicationsController(
            // GraphServiceClient graphClient,
            ILogger<CommunicationsController> logger)
        {
            this._graphClientUser= GraphServiceClientProvider.GetGraphServiceClient();

            this._graphClientDaemon = DaemonRun.GetGraphServiceClient();

            lock (subLoadLock)
            {
                Task.WaitAll(LoadAllSubscriptions());
            }

            //Task.WaitAll(LogCallRecordsToDatabase());
            _logger = logger;
        }


       

        #region API methods

        // GET: api/<CommunicationsController>
        [HttpGet]
        public string Get()
        {

            //net stop hns
            //net start hns
            //ngrok http 44385 -host-header="localhost:44385"
            //if (CommSubscriptions.Count == 0)
            return JsonOfCommSubscriptions();


        }
        private string JsonOfCommSubscriptions()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in CommSubscriptions)
            {
                sb.Append(JsonConvert.SerializeObject(s.Value));
            }

            string res = sb.Length == 0 ? "empty" : sb.ToString();

            return res;
        }


        // GET api/<CommunicationsController>/subscription
        [HttpGet("{recordid}")]
        public async Task<ActionResult<string>> Get(string recordid)
        {
            if (recordid.ToLower() == "createsub")
            {
                var sub = new Microsoft.Graph.Subscription();
                sub.ChangeType = "created";//"created,updated";

                //sub.NotificationUrl = "https://93ba-165-228-138-10.ngrok.io" + "/api/Communications";

                sub.NotificationUrl = "https://msgraph.livingstone.com.au" + "/api/Communications";

                sub.Resource = "/communications/callRecords";
                sub.ExpirationDateTime = DateTime.UtcNow.AddMinutes(4230);
                sub.ClientState = "SecretClientState";

                var newSubscription = await this._graphClientDaemon
                    .Subscriptions
                    .Request()
                    .AddAsync(sub);


                newSubscription.ExpirationDateTime = TimeZoneInfo.ConvertTimeFromUtc(newSubscription.ExpirationDateTime.Value.DateTime, TimeZoneInfo.Local);

                CommSubscriptions[newSubscription.Id] = newSubscription;


                Dictionary<string, object> param = new Dictionary<string, object>() {
                                { "@subscriptionId", newSubscription.Id },
                                 { "@expirationDateTime", newSubscription.ExpirationDateTime.Value.DateTime },
                                { "@content", JsonConvert.SerializeObject(newSubscription)}};
                dBHandler.ExecuteNonQuery("EXEC [MicrosoftGraphSubscriptionUpdate] @subscriptionId, @expirationDateTime, @content", SqlServerDB.Servers.netcrmau, param);



                //string subResult = $"Subscribed. Id: {newSubscription.Id}, Expiration: {newSubscription.ExpirationDateTime}";

                return "subscription is created with id: " + newSubscription.Id;
            }
            else if (recordid.ToLower() == "renewsub")
            {
                CheckSubscriptions(null);

                return JsonOfCommSubscriptions();
            }
            //else if(recordid.ToLower() =="createauto")
            //{
            //    //RecurringJob.AddOrUpdate("test Insert", () => LogCallRecordsToDatabaseTest(), "* 13-14 * * *", TimeZoneInfo.Local);

            //    //recurring of Logging CallRecords To Database
            //    RecurringJob.AddOrUpdate("test", () => LogCallRecordsToDatabaseTest(), Cron.Minutely, TimeZoneInfo.Local);


            //    //recurring of Logging CallRecords To Database
            //    RecurringJob.AddOrUpdate("LogCallRecordsToDatabase", () => LogCallRecordsToDatabase(null), Cron.Minutely, TimeZoneInfo.Local);


            //    //recurring of checking subscription
            //    RecurringJob.AddOrUpdate("CheckSubscriptionsExpiry", () => CheckSubscriptions(null), Cron.MinuteInterval(30), TimeZoneInfo.Local);         

            //    return "hangfire RecurringJob created";
            //}
            else if (recordid.ToLower() == "loop")
            {

                string sql = @"  
 select  a.RecordId
from  [Backup].[dbo].MicrosoftGraphCallRecordsDetails a
where a.RecordParticipantsIds is null
 order by a.ts desc";
                DataTable dt = new DataTable();
                dBHandler.getDataList(dt, sql, SqlServerDB.Servers.netcrmau);

                foreach (DataRow item in dt.Rows)
                {
                    string OneRecordId = item["RecordId"].ToString().Trim();
                    CallRecord record = this._graphClientDaemon.Communications.CallRecords[OneRecordId].Request().GetAsync().Result;
                    CallRecordDataProcess(record);
                }

                return "all ids are processed ";
            }
            else
            {
                CallRecord record = this._graphClientDaemon.Communications.CallRecords[recordid]
                    .Request()
                    .GetAsync()
                    .Result;
                string recordInfo = JsonConvert.SerializeObject(record);

                return recordInfo;

            }
        }

        private void CallRecordDataProcess(CallRecord callRecord)
        {
            IdentitySet organizer = callRecord.Organizer;
            string organiserId = "";

            if (organizer != null)
            {
                if (organizer.User != null)
                {
                    organiserId = organizer.User.Id;
                }
                else if (organizer.AdditionalData["phone"] != null)
                {
                    JObject joPhone = ((JObject)organizer.AdditionalData["phone"]);
                    organiserId = joPhone["id"].ToString().Replace("+", "");
                }
            }


            StringBuilder participantsIdBuilder = new StringBuilder();
            foreach (IdentitySet ids in callRecord.Participants)
            {
                string userId = "";
                if (ids.AdditionalData["phone"] != null)
                {
                    JObject joPhone = ((JObject)ids.AdditionalData["phone"]);
                    userId = joPhone["id"].ToString().Replace("+", "");
                }
                if (ids.AdditionalData["guest"] != null)
                {
                    JObject joPhone = ((JObject)ids.AdditionalData["guest"]);
                    userId = joPhone["id"].ToString();
                }
                if (ids.User != null)
                {
                    userId = ids.User.Id;
                }

                if(participantsIdBuilder.Length>0)
                {
                    participantsIdBuilder.Append(",");
                }
             
                participantsIdBuilder.Append(userId);

                #region  add to callRecord participants table

                Dictionary<string, object> paramP = new Dictionary<string, object>();
                paramP.Add("@recordId", callRecord.Id);
                paramP.Add("@ParticipantId", userId);

                string participantsSql = @"exec [insertMicrosoftGraphCallRecordParticipantsId] @recordId,@ParticipantId";
                dBHandler.ExecuteNonQuery(participantsSql, SqlServerDB.Servers.netcrmau, paramP);
                #endregion
            }

            #region  update organizer id and participants id in the record detail line

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("@recordId", callRecord.Id);
            param.Add("@OrganizerId", organiserId);
            param.Add("@RecordParticipantsIds", participantsIdBuilder.ToString());

            string updateDataSql = @"exec [MicrosoftGraphCallRecordInfoUpdateUsersIdsByRecordId] @recordId, @OrganizerId,@RecordParticipantsIds";
            dBHandler.ExecuteNonQuery(updateDataSql, SqlServerDB.Servers.netcrmau, param);

            #endregion

        }



        // POST api/<CommunicationsController>
        [HttpPost]
        public async Task<ActionResult<string>> Post([FromQuery] string validationToken = null)
        {
            // handle validation
            if (!string.IsNullOrEmpty(validationToken))
            {
                //Debug.WriteLine($"Received Token: '{validationToken}'");
                return Ok(validationToken);
            }


            #region  check subscription expired or not  and logging using Timer
            if (subscriptionTimer == null || recordLoggingTimer == null)
            {
                lock (recordDetailsAndSubsLock)
                {

                    if (subscriptionTimer == null)
                    {
                        subscriptionTimer = new Timer(CheckSubscriptions, null, 5000, 600000);

                    }
                    if (recordLoggingTimer == null)
                    {
                        recordLoggingTimer = new Timer(LogCallRecordsToDatabase, null, 5000, 20000);
                    }
                }
            }
            #endregion

            // handle notifications
            using (StreamReader reader = new StreamReader(Request.Body))
            {
                string content = await reader.ReadToEndAsync();

                //Debug.WriteLine(content);

                var notifications = JsonConvert.DeserializeObject<Notifications>(content);

                if (notifications != null)
                {
                    lock (recordInsertLock)
                    {
                        foreach (var notification in notifications.Items)
                        {
                            //Debug.WriteLine($"Received notification: '{notification.Resource}', {notification.ResourceData?.Id}");

                            DateTime subscriptionExpiredTime = notification.SubscriptionExpirationDateTime.LocalDateTime;

                            if (notification.ResourceData != null)
                            {
                                Dictionary<string, object> param = new Dictionary<string, object>() {{ "@recordId",notification.ResourceData.Id },
                                { "@subscriptionId", notification.SubscriptionId },
                                { "@subscriptionExpiredTime", subscriptionExpiredTime},
                                { "@content", JsonConvert.SerializeObject(notification)}
                            };
                                dBHandler.ExecuteNonQuery("EXEC insertNewMicrosoftGraphCallRecordInfo @recordId, @subscriptionId,@subscriptionExpiredTime, @content", SqlServerDB.Servers.netcrmau, param);
                            }
                        }
                    }
                }
            }

            return Ok();
        }


        // PUT api/<CommunicationsController>/5
        [HttpPut]
        public void Put(int id)
        {
            //CheckSubscriptions();
        }

        // DELETE api/<CommunicationsController>/5
        [HttpDelete]
        public async Task<string> Delete(string id)
        {
            if (id.Length > 2)
            {
                await DeleteSubscription(id);
                return "deleted";
            }

            return "id error";

        }



        #endregion

        #region   Log record details

        //just for testing 
        public async Task LogCallRecordsToDatabaseTest()
        {
            string sql = "insert into[Backup].[dbo].[adrian_test]([Item_code])  values(GETDATE())";
            dBHandler.ExecuteNonQuery(sql, SqlServerDB.Servers.netcrmau);

        }

        //public async Task LogCallRecordsToDatabase()
        //{

        //    DataTable dt = new DataTable();
        //    dBHandler.getDataList(dt, "exec getMicrosoftGraphCallRecords", SqlServerDB.Servers.netcrmau);
        //    if (dt.Rows.Count > 0)
        //    {
        //        string recordid = dt.Rows[0]["recordid"].ToString();
        //        Microsoft.Graph.CallRecords.CallRecord record = _graphClientDaemon.Communications.CallRecords[recordid]
        //            .Request()
        //            .GetAsync().Result;

        //        DateTime startTime = TimeZoneInfo.ConvertTimeFromUtc(record.StartDateTime.Value.DateTime, TimeZoneInfo.Local);
        //        DateTime endTime = TimeZoneInfo.ConvertTimeFromUtc(record.EndDateTime.Value.DateTime, TimeZoneInfo.Local);

        //        string recordOrganizer = JsonConvert.SerializeObject(record.Organizer);
        //        string recordParticipants = JsonConvert.SerializeObject(record.Participants.ToList());
        //        string recordType = record.Type?.ToString();
        //        string recordModalities = JsonConvert.SerializeObject(record.Modalities?.ToList());


        //        Dictionary<string, object> param = new Dictionary<string, object>() {
        //                        { "@recordId",recordid},
        //                        { "@startDateTime", startTime },
        //                        { "@endDateTime", endTime},
        //                         { "@Organizer",recordOrganizer },
        //                        { "@RecordParticipants", recordParticipants},
        //                        { "@Type", recordType},
        //                         { "@Modalities",recordModalities }
        //        };
        //        dBHandler.ExecuteNonQuery("EXEC LogMicrosoftGraphCallRecordtoDB @recordId, @startDateTime, @endDateTime,@Organizer, @RecordParticipants, @Type, @Modalities",
        //            SqlServerDB.Servers.netcrmau, param);
        //    }
        //}

        public void LogCallRecordsToDatabase(Object? stateInfo)
        {

            DataTable dt = new DataTable();
            dBHandler.getDataList(dt, "exec getMicrosoftGraphCallRecords", SqlServerDB.Servers.netcrmau);
            if (dt.Rows.Count > 0)
            {
                string recordid = dt.Rows[0]["recordid"].ToString();
                Microsoft.Graph.CallRecords.CallRecord record=null;
                try
                {
                   record = _graphClientDaemon.Communications.CallRecords[recordid]
                        .Request()
                        .GetAsync().Result;
                }
                catch(Exception e)
                {
                    _logger.LogError(e.ToString());

                }

                if (record != null)
                {
                    DateTime startTime = TimeZoneInfo.ConvertTimeFromUtc(record.StartDateTime.Value.DateTime, TimeZoneInfo.Local);
                    DateTime endTime = TimeZoneInfo.ConvertTimeFromUtc(record.EndDateTime.Value.DateTime, TimeZoneInfo.Local);

                    string recordOrganizer = JsonConvert.SerializeObject(record.Organizer);
                    string recordParticipants = JsonConvert.SerializeObject(record.Participants.ToList());
                    string recordType = record.Type?.ToString();
                    string recordModalities = JsonConvert.SerializeObject(record.Modalities?.ToList());


                    //if(recordParticipants.Length >=3000)
                    //{
                    //    recordParticipants = CallRecordDataProcessService.SimplifyJsonUsers(recordParticipants,out string useridonly);
                    //}


                    Dictionary<string, object> param = new Dictionary<string, object>() {
                                { "@recordId",recordid},
                                { "@startDateTime", startTime },
                                { "@endDateTime", endTime},
                                 { "@Organizer",recordOrganizer },
                                { "@RecordParticipants", recordParticipants},
                                { "@Type", recordType},
                                 { "@Modalities",recordModalities }
                    };
                    dBHandler.ExecuteNonQuery("EXEC LogMicrosoftGraphCallRecordtoDB @recordId, @startDateTime, @endDateTime,@Organizer, @RecordParticipants, @Type, @Modalities",
                        SqlServerDB.Servers.netcrmau, param);

                    CallRecordDataProcess(record);

                }

            }
        }

        #endregion




        #region Subscription Methods

        private async Task LoadAllSubscriptions()
        {
            //Subscription subs1 = _graphClientDaemon.Subscriptions["d7d4e73f-578a-4f28-8dec-f2638ec1fd65"]
            //    .Request().GetAsync().Result;

            if (CommSubscriptions.Count == 0)
            {
                var allsubs = _graphClientDaemon.Subscriptions.Request().GetAsync().Result;

                if (allsubs.Count > 0)
                {
                    do
                    {
                        foreach (var s in allsubs)
                        {
                            if (!string.IsNullOrEmpty(s.Id))
                            {
                                s.ExpirationDateTime = TimeZoneInfo.ConvertTimeFromUtc(s.ExpirationDateTime.Value.DateTime, TimeZoneInfo.Local);

                                CommSubscriptions.Add(s.Id, s);
                            }
                        }
                    } while (allsubs.NextPageRequest != null && (allsubs = await allsubs.NextPageRequest.GetAsync()).Count > 0);
                }
            }

        }


        public void CheckSubscriptions(Object stateInfo)
        {
            //AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;

            //Debug.WriteLine($"Checking subscriptions {DateTime.Now.ToString("h:mm:ss.fff")}");
            //Debug.WriteLine($"Current subscription count {CommSubscriptions.Count()}");

            foreach (var subscription in CommSubscriptions)
            {
                // if the subscription expires in the next 1 day, renew it
                if (subscription.Value.ExpirationDateTime < DateTime.UtcNow.AddMinutes(1000))
                {
                    RenewSubscription(subscription.Value);
                }
            }
        }

        public async void RenewSubscription(Subscription subscription)
        {
            //Debug.WriteLine($"Current subscription: {subscription.Id}, Expiration: {subscription.ExpirationDateTime}");

            var graphServiceClient = this._graphClientDaemon;

            var newSubscription = new Subscription
            {
                ExpirationDateTime = DateTime.UtcNow.AddMinutes(4230)//callRecord Maximum expiration time: 4230 minutes 
            };

            await graphServiceClient
                .Subscriptions[subscription.Id]
                .Request()
                .UpdateAsync(newSubscription);

            //renew your subscription dictionary
            subscription.ExpirationDateTime = TimeZoneInfo.ConvertTimeFromUtc(newSubscription.ExpirationDateTime.Value.DateTime, TimeZoneInfo.Local) ;
            CommSubscriptions[subscription.Id] = subscription;
            //Debug.WriteLine($"Renewed subscription: {subscription.Id}, New Expiration: {subscription.ExpirationDateTime}");

            Dictionary<string, object> param = new Dictionary<string, object>() {
                                { "@subscriptionId", subscription.Id },
                                 { "@expirationDateTime", subscription.ExpirationDateTime.Value.DateTime },
                                { "@content", JsonConvert.SerializeObject(subscription)}};
            dBHandler.ExecuteNonQuery("EXEC [MicrosoftGraphSubscriptionUpdate] @subscriptionId, @expirationDateTime, @content", SqlServerDB.Servers.netcrmau, param);

        }

        private async Task DeleteSubscription(string subid)
        {
            try
            {
                // Get all current subscriptions
                var subscriptions = await this._graphClientDaemon.Subscriptions
                    .Request()
                    .GetAsync();

                foreach (var subscription in subscriptions.CurrentPage)
                {
                    // Delete the subscription
                    await this._graphClientDaemon.Subscriptions[subscription.Id]
                        .Request()
                        .DeleteAsync();

                    // Remove the subscription from the subscription store
                    foreach (var subItem in CommSubscriptions)
                    {
                        if (subItem.Key == subid)
                        {
                            CommSubscriptions.Remove(subid);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error deleting existing subscriptions");
            }
        }

        #endregion


        #region shared method



        #endregion










    }
}
