﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Livingstone.DBLib;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using MicrosoftGraphCore.AuthProcess;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KeyValuePair = System.Collections.Generic.KeyValuePair;

//using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace MicrosoftGraphCore.Controllers
{
    [EnableCors("AllowAnyOrigin")]
    [Route("api/[controller]"),ApiController]
    public class TeamsPresenceController : ControllerBase
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();

        private readonly GraphServiceClient _graphClient;
        private readonly ILogger<TeamsPresenceController> _logger;
        private static volatile Dictionary<string, string> usersIdEmail = new Dictionary<string, string>();
        private static volatile Dictionary<string, string> usersEmail = new Dictionary<string, string>();

        private static readonly object initializeLock = new object();

        private static readonly object reloadLock = new object();

        private static readonly object presenceLock = new object();

        //private PublicAppUsingUsernamePassword tokenAcquisitionHelper;
        //protected ProtectedApiCallHelper protectedApiCallHelper;
        //private string WebApiUrlUsers { get { return $"https://graph.microsoft.com/v1.0/users"; } }
        private static Timer updateUsersPresence;
        private static string usersPresenceJson="";
        public TeamsPresenceController(
            //GraphServiceClient graphClient,
            ILogger<TeamsPresenceController> logger)
        {
           // _graphClient = graphClient;
            _logger = logger;



            #region Try get token by user name and passwrod, resource owner password credentials

            //SampleConfiguration config = SampleConfiguration.ReadFromJsonFile("appsettings.json");
            //var appConfig = config.PublicClientApplicationOptions;
            //var app = PublicClientApplicationBuilder.CreateWithApplicationOptions(appConfig)
            //    .Build();
            //tokenAcquisitionHelper = new PublicAppUsingUsernamePassword(app);

            //string username = "emailcampaign@livingstone.com.au";
            //string password = "Tod76329";
            //string[] scopes1 = new string[] { ".default" };
            //AuthenticationResult authenticationResult =  tokenAcquisitionHelper.AcquireATokenFromCacheOrUsernamePasswordAsync(scopes1, username, password).Result;

            //protectedApiCallHelper = new ProtectedApiCallHelper(new HttpClient());

            //string accessToken = authenticationResult.AccessToken;
            
            //protectedApiCallHelper.CallWebApiAndProcessResult(WebApiUrlUsers, accehttps://msgraph.livingstone.com.au/api/teamspresence/anthony_vasquez@livingstone.com.aussToken);

            #endregion


            #region Try to integrate with graph client

            // Create an authentication provider.

            this._graphClient = GraphServiceClientProvider.GetGraphServiceClient();


            #endregion

           


            if (usersIdEmail == null || usersIdEmail.Count == 0)
            {
                lock (initializeLock)
                {
                    if (usersIdEmail == null || usersIdEmail.Count == 0)
                    {
                        ReloadAllUsersIdEmails();
                    }
                }
            }

            
        }

        [HttpGet]
        public async Task<string> Get()
        {

            if(updateUsersPresence == null)
            {
                updateUsersPresence = new Timer(LoadUsersPresences, null, 5000, 8000);
            }

            LoadUsersPresences("");

            return usersPresenceJson;
        }

        public void LoadUsersPresences(object? state)
        {
            if (usersIdEmail.Count > 0)
            {
                usersPresenceJson = GetPresenceById(usersIdEmail).Result;
            }
        }


        [HttpGet("{email}")]
        //[AuthorizeForScopes(ScopeKeySection = "DownstreamApi:Scopes")]
        public async Task<string> Get(string email)
        {
            string jsonResult = "";

            if (!string.IsNullOrEmpty(email) && usersIdEmail.Count > 0)
            {
                //add reload keyword and load all users' presence. 
                if (email.ToLower() == "reload")
                {
                    ReloadAllUsersIdEmails();

                    jsonResult = GetPresenceById(usersIdEmail).Result;

                }
                else if (usersEmail.ContainsKey(email))
                {
                    Dictionary<string, string> userIdByEmail = usersIdEmail.Where(x => x.Value == email).ToDictionary(x => x.Key, x => x.Value);

                    jsonResult = GetPresenceById(userIdByEmail).Result;
                }
                else
                {
                    jsonResult = "";
                }
            }
            else
            {
                jsonResult = "";
            }

            return jsonResult;
        }

        private void ReloadAllUsersIdEmails()
        {
            #region shared usersIdEmail dictionary for multi-threading

            lock (reloadLock)
            {
                Task.WaitAll(ReloadAllUsersIdEmailsAsync());
            }

            #endregion
        }

        private async Task ReloadAllUsersIdEmailsAsync()
        {
            Dictionary<string, string> TempusersIdEmail = new Dictionary<string, string>();
           

            try
            {
                var users = await _graphClient.Users
                    .Request()
                    .GetAsync();
                do
                {
                    foreach (var u in users)
                    {
                        string result = u.DisplayName;

                        if (u.Mail != null)
                        {

                            TempusersIdEmail.Add(u.Id, u.Mail.ToLower());

                            //insert into database
                            Dictionary<string, object> param = new Dictionary<string, object>() {
                                { "@userId", u.Id },
                                 { "@userMail", u.Mail.ToLower() },
                            { "@displayName", u.DisplayName.ToLower() }};
                            dBHandler.ExecuteNonQuery("EXEC [insertMicrosoftGraphUserInfo] @userId, @userMail, @displayName", SqlServerDB.Servers.netcrmau, param);

                        }
                    }
                } while (users.NextPageRequest != null && (users = await users.NextPageRequest.GetAsync()).Count > 0);


                //if get users id and email, (refresh) copy to usersIdEmail 
                if (TempusersIdEmail.Count > 0)
                {
                    usersIdEmail.Clear();

                    usersIdEmail = TempusersIdEmail;

                    usersEmail.Clear();

                    foreach (var item in usersIdEmail)
                    {
                        usersEmail.Add(item.Value,"");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                throw;
            }




        }

        public async Task<string> GetPresenceById(Dictionary<string, string> idEmailsDic)
        { 
            List<Dictionary<string, string>> idEmailDicList = new List<Dictionary<string, string>>();

            string jsonResult ="";

            //GetPresencesByUserId don't accept id more than 500, so must store in some dictionaries. 
            if (idEmailsDic.Count > 500)
            {

                Dictionary<string, string> idEmailsDicCopied =
                    (from x in idEmailsDic select x).ToDictionary(x => x.Key, x => x.Value);

                do
                {
                    Dictionary<string, string> tempIds = new Dictionary<string, string>();
                    int i = 1;
                    foreach (KeyValuePair<string, string> kv in idEmailsDicCopied)
                    {
                        if ( i <= 500)
                        {
                            i++;
                            tempIds.Add(kv.Key, kv.Value);
                            idEmailsDicCopied.Remove(kv.Key);
                        }
                        else
                        {
                            break;
                        }
                    }

                    idEmailDicList.Add(tempIds);
                } while (idEmailsDicCopied.Count > 0);
            }
            else
            {
                idEmailDicList.Add(idEmailsDic);
            }


            lock(presenceLock)
            {
                jsonResult =GetPresenceByIdDictionaryList(idEmailDicList).Result;
            }
            



            return jsonResult;

        }


        public async Task<string> GetPresenceByIdDictionaryList(List<Dictionary<string, string>> idEmailDicList)
        {
            string jsonResult = "";
            Dictionary<string, string> UsersPresence = new Dictionary<string, string>();
            foreach (Dictionary<string, string> dic in idEmailDicList)
            {
                try
                {
                    var userIDPresences = await _graphClient.Communications
                    .GetPresencesByUserId(dic.Keys)
                    .Request()
                    .PostAsync();
                    do
                    {
                        foreach (var p in userIDPresences)
                        {
                            //add email and availability
                            UsersPresence.Add(dic[p.Id], p.Availability);

                            //update presence in database
                            Dictionary<string, object> param = new Dictionary<string, object>() {
                                { "@userId", p.Id },
                                 { "@Presence", p.Availability.ToLower() }};
                            dBHandler.ExecuteNonQuery("EXEC [updateMicrosoftGraphUserPresences] @userId, @Presence", SqlServerDB.Servers.netcrmau, param);

                        }
                    } while (userIDPresences.NextPageRequest != null &&
                             (userIDPresences = await userIDPresences.NextPageRequest.PostAsync()).Count > 0);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.ToString());
                    throw;
                }
            }

            if (UsersPresence.Count == 1)
            {
                jsonResult = JsonConvert.SerializeObject(new UserInfo(UsersPresence.Keys.FirstOrDefault(), UsersPresence[UsersPresence.Keys.FirstOrDefault()]));
            }
            else if (UsersPresence.Count > 1)
            {
                List<UserInfo> ul = UsersPresence.Select(u => new UserInfo(u.Key, u.Value)).ToList();

                jsonResult = JsonConvert.SerializeObject(ul);
            }
            return jsonResult;
        }











    }

    public class UserInfo
    {
        public string email;
        public string status;

        public UserInfo(string e, string s)
        {
            this.email = e;
            this.status = s;
        }
    }


}
