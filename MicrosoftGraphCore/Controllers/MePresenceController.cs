﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Graph;
using Microsoft.Identity.Web;
using MicrosoftGraphCore.AuthProcess;

namespace MicrosoftGraphCore.Controllers
{
    [Route("api/[controller]"),ApiController]
    public class MePresenceController : ControllerBase
    {
        private readonly GraphServiceClient _graphClient;
        private readonly ILogger<MePresenceController> _logger;

        public MePresenceController(
           // GraphServiceClient graphClient,
            ILogger<MePresenceController> logger)
        {
          
            this._graphClient =  GraphServiceClientProvider.GetGraphServiceClient();

            _logger = logger;
        }


        #region test with current user presence
        [HttpGet]
        // Minimum permission scope needed for this view
        [AuthorizeForScopes(Scopes = new[] { "Presence.Read", "Presence.Read.All" })]
        public async Task<string> MyPresence()
        {

            var pEventPresence = await GetMePresence();

            string myPresence = pEventPresence.Activity;
            return myPresence;
        }
        private async Task<Presence> GetMePresence()
        {

            var events = await _graphClient.Me.Presence
                .Request()
                .GetAsync();

            return events;

        }

        #endregion 



    }
}
