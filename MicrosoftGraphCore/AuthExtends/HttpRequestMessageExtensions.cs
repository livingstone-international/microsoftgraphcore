﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Graph;
using MicrosoftGraphCore.AuthProcess;

namespace MicrosoftGraphCore.AuthExtends
{
    internal static class HttpRequestMessageExtensions
    {
        internal static AuthenticationProviderOption GetMsalAuthProviderOption(this HttpRequestMessage httpRequestMessage)
        {
            AuthenticationHandlerOption authHandlerOption = httpRequestMessage.GetMiddlewareOption<AuthenticationHandlerOption>();
            AuthenticationProviderOption authenticationProviderOption = authHandlerOption?.AuthenticationProviderOption as AuthenticationProviderOption ?? new AuthenticationProviderOption();

            // copy the claims and scopes information
            if (authHandlerOption?.AuthenticationProviderOption is ICaeAuthenticationProviderOption caeAuthenticationProviderOption)
            {
                authenticationProviderOption.Claims = caeAuthenticationProviderOption.Claims;
                authenticationProviderOption.Scopes = caeAuthenticationProviderOption.Scopes;
            }

            return authenticationProviderOption;
        }
    }
}
