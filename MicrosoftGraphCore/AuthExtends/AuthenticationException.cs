﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Graph;

namespace MicrosoftGraphCore.AuthExtends
{
    public class AuthenticationException : Exception
    {
        /// <summary>
        /// Creates a new authentication exception.
        /// </summary>
        /// <param name="error">The error that triggered the exception.</param>
        /// <param name="innerException">The possible inner exception.</param>
        public AuthenticationException(Error error, Exception innerException = null)
            : base(error?.ToString(), innerException)
        {
            this.Error = error;
        }

        /// <summary>
        /// The error from the authentication exception.
        /// </summary>
        public Error Error { get; private set; }
    }
}
