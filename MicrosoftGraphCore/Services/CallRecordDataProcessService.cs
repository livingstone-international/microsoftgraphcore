﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;


namespace MicrosoftGraphCore.Services
{
    public class CallRecordDataProcessService
    {
        private static IDBHandler dBHandler = new SqlServerDB();

        public static void updateCallRecordData()
        {
            DataTable dt = new DataTable();
            string PickDataSql = @"exec [MicrosoftGraphCallRecordInfoPickNull]";
            //pick one
            dBHandler.getDataList(dt, PickDataSql, SqlServerDB.Servers.netcrmau);

            while (dt.Rows.Count > 0)
            {

                int id = Convert.ToInt32(dt.Rows[0]["Id"]);
                string recordId = dt.Rows[0]["RecordId"].ToString();
                string Organizer = dt.Rows[0]["Organizer"].ToString();
                string RecordParticipants = dt.Rows[0]["RecordParticipants"].ToString();

                #region update Organizer

                string simpliedOrganizerId;
                SimplifyJsonUsers(Organizer, out simpliedOrganizerId);
                Organizer = simpliedOrganizerId;

                #endregion

                #region update RecordParticipants

                string simpliedRecordParticipantsIds;
                string simpliedRecordParticipants = SimplifyJsonUsers(RecordParticipants, out simpliedRecordParticipantsIds);
                RecordParticipants = simpliedRecordParticipants;

                #endregion


                #region  add to callRecord participants table 

                if (simpliedRecordParticipantsIds.Length > 2)
                {
                    string[] partIds = simpliedRecordParticipantsIds.Split(',');

                    foreach (var pid in partIds)
                    {
                        Dictionary<string, object> paramP = new Dictionary<string, object>();
                        paramP.Add("@recordId", recordId);
                        paramP.Add("@ParticipantId", pid);

                        string participantsSql = @"exec [insertMicrosoftGraphCallRecordParticipantsId] @recordId,@ParticipantId";
                        dBHandler.ExecuteNonQuery(participantsSql, SqlServerDB.Servers.netcrmau, paramP);
                    }
                }

                #endregion


                #region  update organizer id and participants id in the record detail line

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@Id", id);
                param.Add("@OrganizerId", Organizer);
                param.Add("@RecordParticipantsIds", RecordParticipants);

                string updateDataSql = @"exec [MicrosoftGraphCallRecordInfoUpdateUsersIds] @Id, @OrganizerId,@RecordParticipantsIds";
                dBHandler.ExecuteNonQuery(updateDataSql, SqlServerDB.Servers.netcrmau, param);

                #endregion


                //pick another one
                dBHandler.getDataList(dt, PickDataSql, SqlServerDB.Servers.netcrmau);
            }

        }

        public static string SimplifyJsonUsers(string data, out string dataKeys)
        {
            string phoneKeys;
            data = MatchPhoneUserAndReplace(data, out phoneKeys);


            string userKeys;
            data = MatchIdentityUserAndReplace(data, out userKeys);

            dataKeys = phoneKeys + userKeys;
            if (dataKeys.Length > 0)
            {
                dataKeys = dataKeys.Remove(dataKeys.Length - 1);
            }

            return data;
        }


        public static string MatchIdentityUserAndReplace(string rawValue, out string userKeys)
        {
            string result = rawValue;
            userKeys = "";


            string pattern1 = "{\"user\":{\"displayName\":\".{1,40}\",\"id\":\".{1,50}\",\"@odata.type\":\"microsoft.graph.identity\",\"tenantId\":.{1,50}},\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":null,\"guest\":null,\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";
            string namePatternStart = "{\"displayName\":\".+\"}";


            string pattern2 = "{\"user\":{\"id\":\".{1,50}\",\"@odata.type\":\"microsoft.graph.identity\",\"tenantId\":.{1,50}},\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":null,\"guest\":null,\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";
            string idPatternStart = "{\"id\":\".{10,100}\"tenantId\":.{1,50}}";


            string pattern3 = "{\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":null,\"guest\":{\"id\":\".{1,50}\",\"displayName\":\".{1,40}\",\"tenantId\":null},\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";
            string guestPatternStart = "{\"id\":\".{10,100}\"tenantId\":.{1,50}}";

            string pattern4 = "{\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":{\"id\":\".{1,50}\",\"displayName\":\".{1,40}\",\"tenantId\":null},\"guest\":null,\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";
            string phonePatternStart = "{\"id\":\".{10,100}\"tenantId\":.{1,50}}";



            result = MatchIdentityUserByPattern(pattern1, namePatternStart, ref rawValue, ref userKeys);

            result = MatchIdentityUserByPattern(pattern2, idPatternStart, ref rawValue, ref userKeys);

            result = MatchIdentityUserByPattern(pattern3, guestPatternStart, ref rawValue, ref userKeys);

            result = MatchIdentityUserByPattern(pattern4, phonePatternStart, ref rawValue, ref userKeys);

            return result;
        }

        private static string MatchIdentityUserByPattern(string patternAll, string patternIdentity, ref string rawValue, ref string keyInValue)
        {

            string IdentityInfo = "";
            Regex rgAll = new Regex(patternAll);

            MatchCollection matchCollection = rgAll.Matches(rawValue);

            //only math essential info

            Regex userNameRg = new Regex(patternIdentity);


            string userIdPatern = "\"id\":\".{1,37}\"";
            Regex userIdRg = new Regex(userIdPatern);



            if (matchCollection.Count > 0)
            {
                foreach (var item in matchCollection)
                {
                    //loop through each identity
                    if (!string.IsNullOrWhiteSpace(item.ToString()))
                    {
                        //only keep essential part
                        IdentityInfo = userNameRg.Match(item.ToString()).ToString();

                        //remove odata field
                        IdentityInfo = IdentityInfo.Replace("\"@odata.type\":\"microsoft.graph.identity\",", "");

                        rawValue = rawValue.Replace(item.ToString(), IdentityInfo);

                        keyInValue += userIdRg.Match(IdentityInfo).ToString().Replace("\"id\":\"", "").Replace("\"", "") + ",";

                    }
                }
            }

            return rawValue;

        }


        public static string MatchPhoneUserAndReplace(string rawValue, out string keyInValue)
        {
            string result = rawValue;
            keyInValue = "";

            string patternTest = "{\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":{\"id\":\"893506668\",\"displayName\":null,\"tenantId\":null},\"guest\":null,\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";


            string pattern1 = "{\"@odata.type\":\"microsoft.graph.identitySet\",\"acsUser\":null,\"spoolUser\":null,\"phone\":{\"id\":\"";

            string patternPhone = @"\+{0,1}\d{1,30}";
            string patternPhoneUnavailable = "unavailable";
            string patternPhoneAnonymouse = "anonymous";

            string pattern2 = "\",\"displayName\":null,\"tenantId\":null},\"guest\":null,\"encrypted\":null,\"onPremises\":null,\"acsApplicationInstance\":null,\"spoolApplicationInstance\":null,\"applicationInstance\":null}";


            string patternAll1 = pattern1 + patternPhone + pattern2;
            string patternAll2 = pattern1 + patternPhoneUnavailable + pattern2;
            string patternAll3 = pattern1 + patternPhoneAnonymouse + pattern2;



            result = MatchPhoneUserByPattern(patternAll1, patternPhone, ref result, ref keyInValue);

            result = MatchPhoneUserByPattern(patternAll2, patternPhoneUnavailable, ref result, ref keyInValue);

            result = MatchPhoneUserByPattern(patternAll3, patternPhoneAnonymouse, ref result, ref keyInValue);



            return result;
        }

        private static string MatchPhoneUserByPattern(string patternAll, string patternPhone, ref string rawValue, ref string keyInValue)
        {

            rawValue = rawValue.Replace("?", "").Replace("#", "");

            Regex rgAll = new Regex(patternAll);
            MatchCollection matchCollection = rgAll.Matches(rawValue);
            Regex rgPhone = new Regex(patternPhone);

            if (matchCollection.Count > 0)
            {
                foreach (var item in matchCollection)
                {
                    if (!string.IsNullOrWhiteSpace(item.ToString()))
                    {
                        string phoneNoMatched = rgPhone.Match(item.ToString()).ToString();

                        rawValue = rawValue.Replace(item.ToString(), "{\"phone\":\"" + phoneNoMatched + "\"}");

                        keyInValue += phoneNoMatched.Replace("+", "") + ",";
                    }
                }
            }

            return rawValue;
        }




    }
}
