﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace MicrosoftGraphCore.AuthProcess
{
    internal class GraphAccount : IAccount
    {
        public GraphAccount(GraphUserAccount graphUserAccount)
        {
            if (graphUserAccount != null)
            {
                Username = graphUserAccount.Email;
                Environment = graphUserAccount.Environment;
                HomeAccountId = new AccountId($"{graphUserAccount.ObjectId}.{graphUserAccount.TenantId}", graphUserAccount.ObjectId, graphUserAccount.TenantId);
            }
        }
        public string Username { get; private set; }

        public string Environment { get; private set; }

        public AccountId HomeAccountId { get; private set; }
    }
}
