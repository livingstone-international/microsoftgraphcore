﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace MicrosoftGraphCore.AuthProcess
{
    public static class GraphServiceClientProvider
    {

        private static volatile GraphServiceClient _graphServiceClient;

        private static readonly object Singleton_Lock = new object();

        public static GraphServiceClient GetGraphServiceClient()
        {
            if (_graphServiceClient != null)
            {

                return _graphServiceClient;
            }
            else
            {
                lock (Singleton_Lock)
                {
                    if (_graphServiceClient == null)
                    {

                        // Create a client application. using Client credential provider
                        IConfidentialClientApplication confidentialClientApplication = ConfidentialClientApplicationBuilder
                            .Create("20ba8f8e-e20f-4ebe-b90f-4330bf9a6017")
                            .WithRedirectUri("https://login.microsoftonline.com/common/oauth2/deviceauth")
                            .WithTenantId("7956bf15-5877-4801-b072-de407227f75e")
                            .WithClientSecret("TKC8Q~d6z9g1Byslg8IPyIYrVtTCo9GbxnoP7ba3")
                            .Build();
                        List<string> scopes = new List<string>();
                        scopes.Add("https://graph.microsoft.com/.default");

                        MsalAuthenticationProvider authenticationProvider = new MsalAuthenticationProvider(confidentialClientApplication, scopes.ToArray());

                        _graphServiceClient = new GraphServiceClient(authenticationProvider);

                    }
                }
                return _graphServiceClient;
            }
        }
    }
}
