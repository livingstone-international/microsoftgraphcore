﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace MicrosoftGraphCore.AuthProcess
{
    public class MsalAuthenticationProvider : IAuthenticationProvider
    {
        private static readonly object Singleton_Lock = new object();
        private IConfidentialClientApplication _clientApplication;
        private string[] _scopes;
        private PublicAppUsingUsernamePassword tokenAcquisitionHelper;
        private static volatile AuthenticationResult authenticationResult;
        public MsalAuthenticationProvider(IConfidentialClientApplication clientApplication, string[] scopes)
        {
            _clientApplication = clientApplication;
            _scopes = scopes;
        }

        public async Task AuthenticateRequestAsync(HttpRequestMessage request)
        {
            var token = await GetTokenAsync();
            request.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
        }

        public async Task<string> GetTokenAsync()
        {
            #region Try get token by user name and passwrod, resource owner password credentials

            if (authenticationResult != null && authenticationResult.ExpiresOn > DateTime.Now)
            {
                return authenticationResult.AccessToken;
            }
            else
            {
                //int tid = Thread.CurrentThread.ManagedThreadId;
                lock (Singleton_Lock)
                {
                    if (authenticationResult == null || authenticationResult.ExpiresOn <= DateTime.Now)
                    {
                        SampleConfiguration config = SampleConfiguration.ReadFromJsonFile("appsettings.json");
                        var appConfig = config.PublicClientApplicationOptions;
                        var app = PublicClientApplicationBuilder.CreateWithApplicationOptions(appConfig)
                            .Build();
                        tokenAcquisitionHelper = new PublicAppUsingUsernamePassword(app);

                        string username = "emailcampaign@livingstone.com.au";
                        string password = "MyLivingPass-JHvd89c67@@p";
                        string[] scopes1 = new string[] { ".default" };
                        authenticationResult = tokenAcquisitionHelper.AcquireATokenFromCacheOrUsernamePasswordAsync(scopes1, username, password).Result;

                    }

                }

                return authenticationResult.AccessToken;
            }

            #endregion

            
        }
    }



    public class DaemonMsalAuthenticationProvider : IAuthenticationProvider
    {
        private static readonly object Singleton_Lock = new object();
        private IConfidentialClientApplication _clientApplication;
        private static string[] _scopes;
        private PublicAppUsingUsernamePassword tokenAcquisitionHelper;
        private static volatile AuthenticationResult authenticationResult;
        public DaemonMsalAuthenticationProvider(IConfidentialClientApplication clientApplication, string[] scopes)
        {
            _clientApplication = clientApplication;
            _scopes = scopes;
        }

        public async Task AuthenticateRequestAsync(HttpRequestMessage request)
        {
            var token = await GetTokenAsync();
            request.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
        }

        public async Task<string> GetTokenAsync()
        {
            #region Try get token  

            if (authenticationResult != null && authenticationResult.ExpiresOn > DateTime.Now)
            {
                return authenticationResult.AccessToken;
            }
            else
            {
                //int tid = Thread.CurrentThread.ManagedThreadId;
                lock (Singleton_Lock)
                {
                    if (authenticationResult == null || authenticationResult.ExpiresOn <= DateTime.Now)
                    {
                        
                        try
                        {
                            authenticationResult = _clientApplication.AcquireTokenForClient(_scopes).ExecuteAsync().Result;

                        }
                        catch (MsalServiceException ex) when (ex.Message.Contains("AADSTS70011"))
                        {
                            // Invalid scope. The scope has to be of the form "https://resourceurl/.default"
                            // Mitigation: change the scope to be as expected
                            //Console.ForegroundColor = ConsoleColor.Red;
                            //Console.WriteLine("Scope provided is not supported");
                            //Console.ResetColor();
                        }

                        ////test for httpClient
                        //var httpClient = new HttpClient();
                        //if (!string.IsNullOrEmpty(result.AccessToken))
                        //{
                        //    var defaultRequestHeaders = httpClient.DefaultRequestHeaders;
                        //    if (defaultRequestHeaders.Accept == null ||
                        //        !defaultRequestHeaders.Accept.Any(m => m.MediaType == "application/json"))
                        //    {
                        //        httpClient.DefaultRequestHeaders.Accept.Add(
                        //            new MediaTypeWithQualityHeaderValue("application/json"));
                        //    }
                        //    defaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", result.AccessToken);
                        //}

                    } 
                    return authenticationResult.AccessToken;
                }
                
            }

            #endregion

        }
    }













}
