﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MicrosoftGraphCore.AuthProcess
{
    internal partial class JwtPayload
    {
        [JsonProperty("aud")]
        public Uri Aud { get; set; }

        [JsonProperty("iss")]
        public Uri Iss { get; set; }

        [JsonProperty("app_displayname")]
        public string AppDisplayname { get; set; }

        [JsonProperty("appid")]
        public Guid Appid { get; set; }

        [JsonProperty("deviceid")]
        public Guid Deviceid { get; set; }

        [JsonProperty("family_name")]
        public string FamilyName { get; set; }

        [JsonProperty("given_name")]
        public string GivenName { get; set; }

        [JsonProperty("ipaddr")]
        public string Ipaddr { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("oid")]
        public Guid Oid { get; set; }

        [JsonProperty("scp")]
        public string Scp { get; set; }

        [JsonProperty("tid")]
        public Guid Tid { get; set; }

        [JsonProperty("unique_name")]
        public string UniqueName { get; set; }

        [JsonProperty("upn")]
        public string Upn { get; set; }
    }

}
