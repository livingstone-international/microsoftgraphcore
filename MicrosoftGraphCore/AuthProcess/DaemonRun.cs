﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates; //Only import this if you are using certificate
using System.Threading.Tasks;
using Microsoft.Graph;

namespace MicrosoftGraphCore.AuthProcess
{
    public class DaemonRun
    {

        private static volatile GraphServiceClient _graphServiceClient;

        private static readonly object Singleton_Lock = new object();

        public static GraphServiceClient GetGraphServiceClient()
        {
            if (_graphServiceClient != null)
            {

                return _graphServiceClient;
            }
            else
            {
                lock (Singleton_Lock)
                {
                    if (_graphServiceClient == null)
                    {

                        // Create a client application. using Client credential provider
                        

                        AuthenticationConfig config = AuthenticationConfig.ReadFromJsonFile("appsettings.json");

                        // You can run this sample using ClientSecret or Certificate. The code will differ only when instantiating the IConfidentialClientApplication
                        bool isUsingClientSecret = AppUsesClientSecret(config);

                        // Even if this is a console application here, a daemon application is a confidential client application
                        IConfidentialClientApplication app;

                        if (isUsingClientSecret)
                        {
                            app = ConfidentialClientApplicationBuilder.Create(config.ClientId)
                                .WithClientSecret(config.ClientSecret)
                                .WithAuthority(new Uri(config.Authority))
                                .Build();
                        }

                        else
                        {
                            X509Certificate2 certificate = ReadCertificate(config.CertificateName);
                            app = ConfidentialClientApplicationBuilder.Create(config.ClientId)
                                .WithCertificate(certificate)
                                .WithAuthority(new Uri(config.Authority))
                                .Build();
                        }

                        // With client credentials flows the scopes is ALWAYS of the shape "resource/.default", as the 
                        // application permissions need to be set statically (in the portal or by PowerShell), and then granted by
                        // a tenant administrator. 
                        string[] scopes = new string[] { $"{config.ApiUrl}.default" };



                        DaemonMsalAuthenticationProvider authenticationProvider = new DaemonMsalAuthenticationProvider(app, scopes.ToArray());

                        _graphServiceClient = new GraphServiceClient(authenticationProvider);


                    }
                }
                return _graphServiceClient;
            }
        }

        public static async Task RunAsync()
        {
            AuthenticationConfig config = AuthenticationConfig.ReadFromJsonFile("appsettings.json");

            // You can run this sample using ClientSecret or Certificate. The code will differ only when instantiating the IConfidentialClientApplication
            bool isUsingClientSecret = AppUsesClientSecret(config);

            // Even if this is a console application here, a daemon application is a confidential client application
            IConfidentialClientApplication app;

            if (isUsingClientSecret)
            {
                app = ConfidentialClientApplicationBuilder.Create(config.ClientId)
                    .WithClientSecret(config.ClientSecret)
                    .WithAuthority(new Uri(config.Authority))
                    .Build();
            }

            else
            {
                X509Certificate2 certificate = ReadCertificate(config.CertificateName);
                app = ConfidentialClientApplicationBuilder.Create(config.ClientId)
                    .WithCertificate(certificate)
                    .WithAuthority(new Uri(config.Authority))
                    .Build();
            }

            // With client credentials flows the scopes is ALWAYS of the shape "resource/.default", as the 
            // application permissions need to be set statically (in the portal or by PowerShell), and then granted by
            // a tenant administrator. 
            string[] scopes = new string[] { $"{config.ApiUrl}.default" };

            AuthenticationResult result = null;
            try
            {
                result = await app.AcquireTokenForClient(scopes)
                    .ExecuteAsync();
              
            }
            catch (MsalServiceException ex) when (ex.Message.Contains("AADSTS70011"))
            {
                // Invalid scope. The scope has to be of the form "https://resourceurl/.default"
                // Mitigation: change the scope to be as expected
                //Console.ForegroundColor = ConsoleColor.Red;
                //Console.WriteLine("Scope provided is not supported");
                //Console.ResetColor();
            }


            // call web API
                //getPstnCalls(fromDateTime=2020-11-01,toDateTime=2021-11-01)
                if (result != null)
            {
                var httpClient = new HttpClient();
                var apiCaller = new ProtectedApiCallHelper(httpClient);
                 apiCaller.CallWebApiAndProcessResult($"{config.ApiUrl}v1.0/communications/callRecords/9501595b-975b-43ad-8ff5-5dcee394c7db/sessions", result.AccessToken);
            }
        }

        /// <summary>
        /// Display the result of the Web API call
        /// </summary>
        /// <param name="result">Object to display</param>
        private static void Display(JObject result)
        {
            foreach (JProperty child in result.Properties().Where(p => !p.Name.StartsWith("@")))
            {
                Console.WriteLine($"{child.Name} = {child.Value}");
            }
        }

        /// <summary>
        /// Checks if the sample is configured for using ClientSecret or Certificate. This method is just for the sake of this sample.
        /// You won't need this verification in your production application since you will be authenticating in AAD using one mechanism only.
        /// </summary>
        /// <param name="config">Configuration from appsettings.json</param>
        /// <returns></returns>
        private static bool AppUsesClientSecret(AuthenticationConfig config)
        {
            string clientSecretPlaceholderValue = "[Enter here a client secret for your application]";
            string certificatePlaceholderValue = "[Or instead of client secret: Enter here the name of a certificate (from the user cert store) as registered with your application]";

            if (!String.IsNullOrWhiteSpace(config.ClientSecret) && config.ClientSecret != clientSecretPlaceholderValue)
            {
                return true;
            }

            else if (!String.IsNullOrWhiteSpace(config.CertificateName) && config.CertificateName != certificatePlaceholderValue)
            {
                return false;
            }

            else
                throw new Exception("You must choose between using client secret or certificate. Please update appsettings.json file.");
        }

        private static X509Certificate2 ReadCertificate(string certificateName)
        {
            if (string.IsNullOrWhiteSpace(certificateName))
            {
                throw new ArgumentException("certificateName should not be empty. Please set the CertificateName setting in the appsettings.json", "certificateName");
            }
            CertificateDescription certificateDescription = CertificateDescription.FromStoreWithDistinguishedName(certificateName);
            DefaultCertificateLoader defaultCertificateLoader = new DefaultCertificateLoader();
            defaultCertificateLoader.LoadIfNeeded(certificateDescription);
            return certificateDescription.Certificate;
        }
    }
}
